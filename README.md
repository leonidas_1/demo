# Utilisation :
1. Clonez ce dépôt sur votre machine locale.
2. Assurez-vous d'avoir Terraform installé localement.
3. Assurez-vous d'avoir vos clefs AWS configuré
4. Assurez-vous d'avoir le bon terraform env
5. Modifiez les variables dans les fichiers Terraform selon vos besoins (par exemple, les plages CIDR, les zones de disponibilité, etc.).
6. Exécutez `terraform init` pour initialiser votre environnement Terraform.
7. Exécutez `terraform plan` pour voir les changements qui seront appliqués.
8. Exécutez `terraform apply` pour déployer l'infrastructure sur AWS.

### Notes :
- Assurez-vous d'avoir les autorisations nécessaires dans votre compte AWS pour créer ces ressources.
- Faites attention aux coûts associés à ces ressources, en particulier le NAT Gateway, qui peut entraîner des frais élevés en fonction de son utilisation.




# Infrastructure Terraform  pour un VPC AWS

Ce projet Terraform déploie une infrastructure de réseau de base sur AWS, en utilisant des ressources telles que des VPC, des sous-réseaux publics et privés, des groupes de sous-réseaux pour les bases de données, une passerelle Internet, des tables de routage et un NAT Gateway. Voici une vue d'ensemble des composants déployés :

### Composants clés :

#### VPC (Virtual Private Cloud)
- Une VPC est créée avec le bloc CIDR spécifié.
- La résolution DNS et les noms de domaine sont activés.

#### Sous-réseaux Publics et Privés
- Des sous-réseaux sont créés dans la VPC pour chaque zone de disponibilité (AZ) spécifiée.
- Les sous-réseaux publics ont la capacité de mapper les adresses IP publiques automatiquement lors du lancement d'instances EC2.
- Les sous-réseaux privés ont également cette capacité, bien que cela ne soit pas utilisé car les instances dans ces sous-réseaux n'auront pas d'adresses IP publiques.

#### Groupe de sous-réseaux pour les bases de données
- Un groupe de sous-réseaux est créé pour les bases de données, et les sous-réseaux privés sont spécifiés pour y être inclus.

#### Passerelle Internet
- Une passerelle Internet est attachée à la VPC pour permettre aux ressources dans la VPC d'accéder à Internet et d'être accessibles depuis Internet.

#### Tables de routage
- Des tables de routage sont créées pour les sous-réseaux publics et privés.
- La table de routage publique est associée à la passerelle Internet pour permettre aux ressources des sous-réseaux publics d'accéder à Internet.
- La table de routage privée est associée au NAT Gateway pour permettre aux ressources des sous-réseaux privés d'accéder à Internet tout en masquant leurs adresses IP privées.

#### NAT Gateway
- Un NAT Gateway est créé dans le sous-réseau public pour permettre aux instances des sous-réseaux privés d'accéder à Internet tout en masquant leurs adresses IP privées.


Voici un exemple de fichier README pour accompagner le code Terraform que vous avez fourni :






# Architecture remote backend-states

Ce déploiement utilise Terraform pour créer les ressources suivantes dans AWS :

- **Table DynamoDB pour les verrous de Terraform** : Cette table est utilisée pour la gestion des verrous de Terraform, permettant ainsi d'assurer l'exclusivité lors du déploiement de l'infrastructure.
  
- **Bucket S3 pour les états Terraform** : Ce bucket S3 est utilisé pour stocker les états Terraform de manière sécurisée et fiable.


## Configuration

Avant de déployer l'infrastructure, vous devrez configurer certaines variables selon vos besoins spécifiques :

- **Variables AWS** : Assurez-vous de définir correctement votre région AWS dans le fichier `main.tf` sous la section `Provider`. Vous pouvez également ajuster d'autres paramètres AWS au besoin.

- **Nom du Bucket S3** : Dans le fichier `main.tf`, remplacez la valeur `your-key-name` par le nom souhaité pour votre bucket S3.

- **Tags et configurations supplémentaires** : Vous pouvez modifier les tags et les configurations des ressources selon vos préférences.




# Récupération des AMIs Ubuntu avec Terraform

Ce référentiel contient des fichiers Terraform pour récupérer les IDs des AMIs Ubuntu disponibles dans AWS pour différentes versions d'Ubuntu.


## Architecture

Ce déploiement utilise Terraform pour récupérer les IDs des AMIs Ubuntu disponibles dans AWS pour les versions suivantes :

- Ubuntu 22.04 (nom de code : Jammy)
- Ubuntu 20.04 (nom de code : Focal Fossa)

Pour chaque version, il récupère les AMIs pour les architectures x86 (amd64) et ARM (arm64).


## Configuration

Aucune configuration supplémentaire n'est nécessaire pour ce script, mais assurez-vous que votre environnement Terraform est correctement configuré pour accéder à AWS.




# Récupération des paires de clés AWS avec Terraform

Ce référentiel contient des fichiers Terraform pour récupérer les informations sur les paires de clés AWS existantes.


## Architecture

Ce déploiement utilise Terraform pour récupérer les informations sur les paires de clés AWS existantes en fonction des critères de recherche spécifiés.

## Configuration

Aucune configuration supplémentaire n'est nécessaire pour ce script, mais assurez-vous que votre environnement Terraform est correctement configuré pour accéder à AWS.