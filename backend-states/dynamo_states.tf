##############################################################################################################
#### DynamoDB lock ####

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-states-locks-db"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  tags         = { "Name" = "Terraform states lock table" }

  attribute {
    name = "LockID"
    type = "S"
  }
}