##############################################################################################################
#### VPC ####

resource "aws_vpc" "organization" {
  cidr_block              = var.vpc_ip[terraform.workspace]
  enable_dns_hostnames    = true
  enable_dns_support      = true

  tags = { Name = "Organization - ENV ${title(terraform.workspace)}" }  
}

##############################################################################################################
#### Public & Private Subnets #####

resource "aws_subnet" "public" {
  count                   = length(var.aws_zone)    #length determines the length of a given list, map, or string.
  vpc_id                  = aws_vpc.organization.id
  cidr_block              = cidrsubnet(var.vpc_ip[terraform.workspace], var.public_extend_prefix, var.public_netnum[count.index] )
  availability_zone       = var.aws_zone[count.index]
  map_public_ip_on_launch = true
  tags                    = { Name = "Public IP - ${var.aws_zone[count.index]} - ENV ${title(terraform.workspace)}" }
}

resource "aws_subnet" "private" {
  count                   = length(var.aws_zone)    #length determines the length of a given list, map, or string.
  vpc_id                  = aws_vpc.organization.id
  cidr_block              = cidrsubnet(var.vpc_ip[terraform.workspace], var.private_extend_prefix, var.private_netnum[count.index] )
  availability_zone       = var.aws_zone[count.index]
  map_public_ip_on_launch = true
  tags                    = { Name = "Private IP - ${var.aws_zone[count.index]} - ENV ${title(terraform.workspace)}" }
}

### DB subnet ###
resource "aws_db_subnet_group" "private_group" {
  name       = "private-subnet-group"
  subnet_ids = [aws_subnet.private[0].id, aws_subnet.private[1].id]
  tags       = { Name = "Private subnet group" }
}


##############################################################################################################
#### Internet Gateway ####

resource "aws_internet_gateway" "organization" {
  vpc_id   = aws_vpc.organization.id
  tags     = { Name = "Internet Gateway - ENV ${title(terraform.workspace)}" }
}

##############################################################################################################
#### Route tables ####

### Public ###

resource "aws_route_table" "public" {
  vpc_id   = aws_vpc.organization.id
  tags     = {"Name" = "Public route - ENV ${title(terraform.workspace)}"}

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.organization.id
  }
}

resource "aws_route_table_association" "public" {
  count          = length(var.aws_zone)
  subnet_id      = aws_subnet.public.*.id[count.index]
  route_table_id = aws_route_table.public.id
}





### Private ###


# Elastic IP for NAT gateway
resource "aws_eip" "elastic_ip" {
  depends_on = [aws_internet_gateway.organization]
  vpc        = true
  tags = {
    Name = "EIP_for_NAT"
  }
}

# NAT gateway for private subnets 
# (for the private subnet to access internet - eg. ec2 instances downloading softwares from internet)
resource "aws_nat_gateway" "private_subnet" {
  allocation_id = aws_eip.elastic_ip.id
  subnet_id     = aws_subnet.public[0].id # nat should be in public subnet

  tags = {
    Name = "NAT for private subnet"
  }

  depends_on = [aws_internet_gateway.organization]
}



# Route table - connecting to NAT
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.organization.id
  tags     = {"Name" = "Private route - ENV ${title(terraform.workspace)}"}

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.private_subnet.id
  }
}


# Associate the route table with private subnet
resource "aws_route_table_association" "private" {
  count          = length(var.aws_zone)
  subnet_id      = aws_subnet.private.*.id[count.index]
  route_table_id = aws_route_table.private.id
}