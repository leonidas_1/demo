##############################################################################################################
#### VPC ####

output "output_aws_vpc_organization_vpc" {
  value       = aws_vpc.organization.id
}

##############################################################################################################
#### Public subnets ####

output "output_public_subnets" {
  value       = aws_subnet.public[*].id
}

##############################################################################################################
#### Private subnets ####

output "output_private_subnets" {
  value       = aws_subnet.private[*].id
}

output  "output_aws_db_subnet_group_private_group" {
  value       = aws_db_subnet_group.private_group.name
}

