##############################################################################################################
#### VPC ####

#variable "vpc_ip" {
#  type    = string
#  default = "10.152.0.0/16"
#}

variable "vpc_ip" {
  type = map
  default = {
    dev      = "10.152.0.0/16"
    stagging = "10.153.0.0/16"
    prod     = "10.154.0.0/16"
  }
}
##############################################################################################################
#### AZ ####

variable "aws_zone" {
  type    = list(string)
  default = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}

##############################################################################################################
#### Public subnets ####

variable "public_extend_prefix" {
  type    = string
  default = "8"
}

variable "public_netnum" {
  type    = list(number)
  default = [100, 150, 200]
}

###############################################################################################################
##### Private subnets ####

variable "private_extend_prefix" {
  type    = string
  default = "4"
}

variable "private_netnum" {
  type    = list(number)
  default = [1, 2, 3]
}