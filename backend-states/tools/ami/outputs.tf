##############################################################################################################
#### VPC ####

output "output_ami_ubuntu_jammy" {
  value       = data.aws_ami.ubuntu-jammy.id
}

output "output_ami_ubuntu_jammy_arm" {
  value       = data.aws_ami.ubuntu-jammy-arm.id
}

output "output_ami_ubuntu_focal" {
  value       = data.aws_ami.ubuntu-focal.id
}

output "output_ami_ubuntu_focal_arm" {
  value       = data.aws_ami.ubuntu-focal-arm.id
}