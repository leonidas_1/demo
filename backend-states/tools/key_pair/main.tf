##############################################################################################################
#### Key pairs ####

data "aws_key_pair" "key_pair" {
  key_name = "your-key-name"          ### replace with your key name
  filter {
    name   = "tag:Name"
    values = ["your-value-key-name"]  ### replace with your value key name
  }
}